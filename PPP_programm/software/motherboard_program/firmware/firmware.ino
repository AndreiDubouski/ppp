//libraries
#include <AccelStepper.h>
#include "xaar128.h"

#define commandSize 128
#define printStartPos 3630

//RLE && COMMAND PROCESSING VARIABLES//
int nozzleRLE[128][20];
char command[commandSize];
int serialCount;
byte cA;
byte cAn;
byte ncPos[128];
int nCount[128];

byte nozzleNum;
byte nComb1[8];
byte nComb2[8];
char temp[3];
bool hasData;

//MOTOR CONTROL VARIABLES//
int standX = 11; //standbay mode
int enX = 8;
int stepX = 9;
int dirX = 10;

int standY = 13; //standbay mode
int enY = 7;
int stepY = 6;
int dirY = 5;

int standZ = 13; //standbay mode
int enZ = 7;
int stepZ = 6;
int dirZ = 5;

AccelStepper stepperY(1, stepY, dirY); // pin stepY = step, pin dirY = direction
AccelStepper stepperZ(1, stepZ, dirZ); // pin stepZ = step, pin dirZ = direction
AccelStepper stepperX(1, stepX, dirX); // pin stepY = step, pin dirY = direction

volatile int Z_POSITION = 0; // Позиция мотора Z
volatile int X_POSITION = 0; // Позиция мотора X
volatile int Y_POSITION = 0; // Позиция мотора Y

const int Z_LIMIT = 3630; //лимит стола, начальная позиция
const int X_LIMIT = 257984; //лимит стола, начальная позиция
const int Y_LIMIT = 257984; //лимит стола, начальная позиция
//MOTOR CONTROL VARIABLES//

Xaar128 xaar128;

//UV-LED//
const int LED = 25;
bool set_led = true;

/////////////        Sensors       //////////////
int END_Z = 43; //вертикальный датчик, если лог. ноль ("0"), то есть перекрытие
int END_X = 41; //горизонтальный датчик, если лог. ноль ("0"), то есть перекрытие
int END_Y = 31; //вертикальный лазерный датчик up/down - sensor
/////////////       Sensors       ///////////////

// init main board
void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, 0);   // off UV-LED

  //MOTOR INIT//
  pinMode(standX, OUTPUT);
  digitalWrite(standX, 0); //удержание
  pinMode(standZ, OUTPUT);
  digitalWrite(standZ, 0); //удержание
  pinMode(standY, OUTPUT);
  digitalWrite(standY, 0); //удержание
  stepperZ.setMaxSpeed(250);
  stepperZ.setAcceleration(50);

  stepperZ.setMaxSpeed(250);
  stepperZ.setAcceleration(50);
  stepperX.setMaxSpeed(8000);
  stepperX.setAcceleration(500000);
  stepperY.setMaxSpeed(4000); //ex 6400
  stepperY.setAcceleration(50000); //ex 40000

  //Sensors//
  pinMode(END_Z, INPUT);
  pinMode(END_X, INPUT);
  pinMode(END_Y, INPUT);

  // delay(500);
  stepperX.setCurrentPosition(0);
  stepperX.moveTo(16);
  while (stepperX.isRunning()) {
    stepperX.run();
  }
  stepperX.setCurrentPosition(0);
  stepperX.moveTo(0);
  digitalWrite(standX, 1); // шагание

  // быстрый поиск датчика
  stepperX.moveTo((X_LIMIT) + 500); // 5% запаса

  while (!digitalRead(END_X))
  { //ищем датчик
    if (stepperX.currentPosition() == stepperX.targetPosition())
    {
      break;
    }
    stepperX.run();
  }
  stepperX.setAcceleration(10000);
  //////датчик перекрыт
  stepperX.stop();
  while (stepperX.isRunning()) {
    stepperX.run();
  }
  {
    stepperX.run();
  }
  stepperX.setCurrentPosition(0);
  stepperX.moveTo(0);
  stepperX.setMaxSpeed(1000);
  stepperX.setAcceleration(1000);
  stepperX.moveTo(stepperX.currentPosition() - 200);
  while (stepperX.isRunning()) {
    stepperX.run(); //отъезжаем
  }
  //нашли датчик быстро
  //задали позицию отъехать
  stepperX.moveTo(stepperX.currentPosition() + 300);

  while (!digitalRead(END_X))
  { //ищем датчик
    if (stepperX.currentPosition() == stepperX.targetPosition())
    {
      break;
    }
    stepperX.run();
  }

  stepperX.moveTo(stepperX.currentPosition() - 20);

  while (stepperX.isRunning()) {
    stepperX.run(); //отъезжаем
  }
  //возвращаем стандартную скорость

  stepperX.setCurrentPosition(X_POSITION); //задаем нулевую позицию
  stepperX.setMaxSpeed(2400);
  stepperX.setAcceleration(10000);

  digitalWrite(standX, 0); // удержание
  stepperX.disableOutputs();
  delay(300);
  stepperX.enableOutputs();

  // put your setup code here, to run once:
  Serial.begin(250000);
  //HEAD INIT//
  xaar128.init();
  TCCR1A = _BV(COM1A0);              // toggle OC1A on compare match
  OCR1A = 7;                         // top value for counter
  // UNO //TCCR1B = _BV(WGM12) | _BV(CS10);   // CTC mode, prescaler clock/1
  TCCR1B = _BV(WGM12) | _BV(CS10);   // CTC mode, prescaler clock/1
}

void loop() {

  if (stepperX.currentPosition() != X_POSITION) {
    digitalWrite(standX, HIGH);
    delay(1000);
  }
  else {
    digitalWrite(standX, LOW);
  }
  if (stepperY.currentPosition() != Y_POSITION) {
    digitalWrite(standY, LOW);
    delay(1000);
  }
  else {
    digitalWrite(standY, HIGH);
  }
  char c;
  if (Serial.available()) {
    c = Serial.read();
    if (c != '\n') {
      command[serialCount] = c;
      serialCount++;
    } else {
      processCommand(command);
      serialCount = 0;
      memset(&command[0], 0, sizeof(command));
    }
  }
}

void newLayer() {
  digitalWrite(6, 0);
  digitalWrite(26, 1);
  delay(500);
  Y_POSITION += 20;
  X_POSITION += 40;
  Serial.println(Y_POSITION);
  Serial.println(X_POSITION);
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
  digitalWrite(standX, 1);
  digitalWrite(standZ, 0);
  digitalWrite(standY, 1);
  delay(100);
  stepperX.setSpeed(1000);
  stepperX.runToNewPosition(-15500);
  delay(100);
  stepperX.runToNewPosition(0);
  digitalWrite(3, 0);
  for (byte n = 0; n < 128; n++) {
    for (int p = 0; p < 20; p++) {
      nozzleRLE[n][p] = 0;
    }
  }
  Serial.println('A');
}

void printLayer() {
  int fails = 0;
  int steps = 0;
  xaar128.powerUp();
  stepperX.setSpeed(200);
  stepperX.runToNewPosition(positions[2]);

  for (int a = 0; a < 128; a++) {
    nCount[a] = nozzleRLE[a][0];
  }
  for (int x = 0; x < 735; x++) {
    hasData = false;
    for (int ca = 0; ca < 8; ca++) {
      for (int cb = 0; cb < 8; cb++) {
        int n = ca * 8 + cb;
        if (nCount[n] == 0) {
          ncPos[n]++;
          nCount[n] = nozzleRLE[n][ncPos[n]] - 1;
          if (x > 0) {
            nComb1[ca] ^= 1 << cb;
          }
        } else if (nCount[n] > 0) {
          nCount[n]--;
          hasData = true;
        }
        int n2 = n + 64;
        if (nCount[n2] == 0) {
          ncPos[n2]++;
          nCount[n2] = nozzleRLE[n2][ncPos[n2]] - 1;
          if (x > 0) {
            nComb2[ca] ^= 1 << cb;
          }
        } else if (nCount[n2] > 0) {
          nCount[n2]--;
          hasData = true;
        }
      }
    }
    if (!hasData) {
      break;
    }
    ///////DATA TO HEAD/////
    for (int r = 0; r < 4; r++) {
      xaar128.loadData(nComb1, nComb2);
      delayMicroseconds(1000);
      xaar128.fire();
    }
    X_POSITION = X_POSITION - 9;
    positions[2] =  (int)roundf(X_POSITION);
    stepperX.runToNewPosition(positions[2]);
  }
  delay(100);
  xaar128.powerDown();
  positions[2] = 0;
  stepperX.runToNewPosition(positions[2]);
  memset(nComb1, 0, 8);
  memset(nComb2, 0, 8);
  memset(nCount, 0, 128);
  memset(ncPos, 0, 128);
  Serial.println('A');
}


void processCommand(char nData[]) {
  if (nData[0] == 'N') {
    //
    temp[0] = nData[1];
    temp[1] = nData[2];
    temp[2] = nData[3];
    nozzleNum = atoi(temp);
    memset(&temp, 0, sizeof(temp));
    cA = 0;
    cAn = 0;
    for (int i = 5; i < commandSize; i++) {
      if (nData[i] != ',' && nData[i] != ']') {
        temp[cA] = nData[i];
        cA++;
      } else {
        nozzleRLE[nozzleNum][cAn] = atoi(temp);
        cAn++;
        cA = 0;
        memset(&temp[0], 0, sizeof(temp));
      }
    }
  }
  // get some data
  if (nData[0] == 'P') {
    for (int n = 0; n < 128; n++) {
      for (int p = 0; p < 20; p++) {
        Serial.print(nozzleRLE[n][p]);
        Serial.print(':');
      }
      Serial.println(n);
    }
  }
  //clean buffer
  if (nData[0] == 'C') {
    for (byte n = 0; n < 128; n++) {
      for (int p = 0; p < 20; p++) {
        nozzleRLE[n][p] = 0;
      }
    }
  }
  // move to start and clean buffer
  if (nData[0] == 'S') {
    printLayer();
    for (byte n = 0; n < 128; n++) {
      for (int p = 0; p < 20; p++) {
        nozzleRLE[n][p] = 0;
      }
    }
  }
  // create new layer
  //смещение по оси Z и подготовка к новому слою
  if (nData[0] == 'L') {
    newLayer();
  }
  // set on off UV-led
  if (nData[0] == 'U') {
    if set_led == true{
    set_led = false;
    pinMode(LED, INPUT);   // off UV-LED
    } else {
      set_led = false;
      pinMode(LED, OUTPUT);   // on UV-LED
    }
  }
  // write data to buffer
  if (nData[0] == 'W') {
    byte w[8] = {255, 255, 255, 255, 255, 255, 255, 255};
    xaar128.powerUp();
    delay(1000);
    for (int r = 0; r < 1000; r++) {
      Serial.println(r);
      xaar128.loadData(w, w);
      delay(1);
      xaar128.fire();
      delay(10);
    }
    xaar128.powerDown();

  }
  // move to position for X
  if (nData[0] == 'X') {
    temp[0] = nData[1];
    temp[1] = nData[2];
    temp[2] = nData[3];
    temp[3] = nData[4];
    temp[4] = nData[5];
    temp[5] = nData[6];
    X_POSITION = atoi(temp);
    memset(&temp[0], 0, sizeof(temp));
    Serial.println(X_POSITION);
    digitalWrite(standX, 0);
    delay(500);
    stepperX.runToNewPosition(X_POSITION);
    digitalWrite(standX, 1);
  }
  // move to position for Z
  if (nData[0] == 'Z') {
    temp[0] = nData[1];
    temp[1] = nData[2];
    temp[2] = nData[3];
    temp[3] = nData[4];
    temp[4] = nData[5];
    temp[5] = nData[6];
    Z_POSITION = atoi(temp);
    memset(&temp[0], 0, sizeof(temp));
    Serial.println(Z_POSITION);
    digitalWrite(standZ, 0);
    delay(500);
    stepperZ.runToNewPosition(Z_POSITION);
    digitalWrite(standZ, 1);
  }
  // move to position for Y
  if (nData[0] == 'Y') {
    temp[0] = nData[1];
    temp[1] = nData[2];
    temp[2] = nData[3];
    temp[3] = nData[4];
    temp[4] = nData[5];
    temp[5] = nData[6];
    Y_POSITION = atoi(temp);
    memset(&temp[0], 0, sizeof(temp));
    Serial.println(Y_POSITION);
    digitalWrite(standY, 0);
    delay(500);
    stepperY.runToNewPosition(Y_POSITION);
    digitalWrite(standY, 1);
  }
}
