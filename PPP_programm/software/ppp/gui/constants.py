import os

app_name = "Put Pen to Paper"
# icon downloaded from url http://www.iconarchive.com/show/colorful-long-shadow-icons-by-graphicloads/Drop-icon.html
icon_path = os.path.join(os.path.abspath(os.path.join('', os.pardir)), 'res\icon.ico')
recent_files_path = os.path.join(os.path.abspath(os.path.join('', os.pardir)), 'res\\recent_files')
version = '0.0.2b'

size = (800, 530)
