"""
This file is part of the PPP suite.

PPP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PPP.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import traceback
import threading
import os
from gui import constants as const

try:
    import simplejson as json
except ImportError:
    import json

try:
    import wx
    import wx.adv

    if wx.VERSION < (4,):
        raise ImportError()
except:
    logging.error("WX >= 4 is not installed. This program requires WX >= 4 to run.")
    raise

# from widgets import PrinterOptions
import widgets


class MainWindow(wx.Frame):
    """
    Main GUI class, consist all UI
    """

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.panel = wx.Panel(self, -1)
        self.reset_ui()
        self.stateful_controls = []

    def reset_ui(self):
        """
        for render UI
        :return:
        """
        self.panels = []
        self.printer_controls = []


class FullWindow(MainWindow):
    """
    Front-end for the All Application.
    """

    def __init__(self, filename=None, size=const.size):
        # self.app = app
        self.filename = filename
        self.window_ready = False
        self.ui_ready = False

        # size = (self.settings.last_window_width, self.settings.last_window_height)
        MainWindow.__init__(self, None, title=const.app_name, size=size)
        logging.info(const.icon_path)
        ico = wx.Icon(const.icon_path, wx.BITMAP_TYPE_ICO)
        self.SetIcon(ico)

        self.window_ready = True
        self.Bind(wx.EVT_CLOSE, self.close_win)

        self.menu_strip = wx.MenuBar()
        self.reload_ui()
        self.update_recent_files()

        self.status_bar = self.CreateStatusBar()
        self.status_bar.SetStatusText("Not connected to printer.")

    #  --------------------------------------------------------------
    #  Main Front-end interface handling
    #  --------------------------------------------------------------


    def close_win(self, event):
        """close app"""
        # TODO add 'pause and resume'
        event.StopPropagation()
        # self.do_exit("force")
        logging.info("force exit")
        exit()

    def reload_ui(self):
        """Create UI"""
        self.create_menu()
        # self.createTabbedGui()

    def create_menu(self):
        """
        Create main stripe menu
        menu:
            -open
            -save
            -recent files
            -exit
        """

        # File menu
        menu = wx.Menu()
        self.Bind(wx.EVT_MENU, self.load_file, menu.Append(-1, "&Open...", " Open file"))
        self.save_btn = menu.Append(-1, "&Save...", " Save file")
        self.save_btn.Enable(False)  # just now
        # self.Bind(wx.EVT_MENU, self.savefile, self.savebtn)
        self.file_history = wx.FileHistory(maxFiles=8, idBase=wx.ID_FILE1)

        recent = wx.Menu()
        self.file_history.UseMenu(recent)
        self.Bind(wx.EVT_MENU_RANGE, self.load_recent_file, id=wx.ID_FILE1, id2=wx.ID_FILE9)
        menu.Append(wx.ID_ANY, "&Recent Files", recent)
        self.Bind(wx.EVT_MENU, self.on_exit, menu.Append(wx.ID_EXIT, "E&xit"), " Closes the Window")
        self.menu_strip.Append(menu, "&File")

        # Tools Menu
        tool_menu = wx.Menu()
        # self.Bind(wx.EVT_MENU, self.do_editgcode, tool_menu.Append(-1, "&Edit...", " Edit open file"))
        # self.Bind(wx.EVT_MENU, self.plate, tool_menu.Append(-1, "Plater", " Compose 3D models into a single plate"))
        # self.Bind(wx.EVT_MENU, self.plate_gcode, tool_menu.Append(-1, "G-Code Plater", " Compose G-Codes into a single plate"))
        # self.Bind(wx.EVT_MENU, self.exclude, tool_menu.Append(-1, "Excluder", " Exclude parts of the bed from being printed"))
        # self.Bind(wx.EVT_MENU, self.project, tool_menu.Append(-1, "Projector", " Project slices"))
        # self.Bind(wx.EVT_MENU, self.show_spool_manager, tool_menu.Append(-1, "Spool Manager", " Manage different spools of filament"))
        self.menu_strip.Append(tool_menu, "&Tools")

        # Settings menu
        settings_menu = wx.Menu()
        self.macros_menu = wx.Menu()
        self.Bind(
            wx.EVT_MENU,
            lambda *e: PrinterOptions(self),
            settings_menu.Append(-1, "&Options", " Options dialog")
        )
        self.Bind(
            wx.EVT_MENU,
            lambda x: threading.Thread(target=lambda: self.do_slice("set")).start(),
            settings_menu.Append(-1, "Slicing settings", " Adjust slicing settings")
        )

        m_item = settings_menu.AppendCheckItem(
            -1,
            "Debug communications",
            "Print all G-code sent to and received from the printer.")
        # menu.Check(m_item.GetId(), self.p.loud)
        # self.Bind(wx.EVT_MENU, self.set_verbose_communications, m_item)

        m_item = settings_menu.AppendCheckItem(
            -1,
            "Don't autoscroll",
            "Disables automatic scrolling of the console when new text is added")
        # menu.Check(m_item.GetId(), self.autoscrolldisable)
        # self.Bind(wx.EVT_MENU, self.set_autoscrolldisable, m_item)

        self.menu_strip.Append(settings_menu, "&Settings")
        # self.update_macros_menu()
        self.SetMenuBar(self.menu_strip)

        help_menu = wx.Menu()
        self.Bind(wx.EVT_MENU, self.about, help_menu.Append(-1, "&About PPP", "Show about dialog"))
        self.menu_strip.Append(help_menu, "&Help")

    def load_file(self, event, filename=None):
        """
        select file with 3D-model
        :param event:
        :param filename:
        :return:
        """
        base_dir = "."
        dlg = None
        if filename is None:
            dlg = wx.FileDialog(
                self,
                "Open file to print",
                base_dir,
                style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            dlg.SetWildcard(
                "OBJ, STL, and GCODE files (*.gcode;*.gco;*.g;*.stl;*.STL;*.obj;*.OBJ)|*.gcode;*.gco;*.g;*.stl;*.STL;*.obj;*.OBJ|GCODE files (*.gcode;*.gco;*.g)|*.gcode;*.gco;*.g|OBJ, STL files (*.stl;*.STL;*.obj;*.OBJ)|*.stl;*.STL;*.obj;*.OBJ|All Files (*.*)|*.*")
            """try:
                dlg.SetFilterIndex(self.settings.last_file_filter)
            except:
                pass"""

        if filename or dlg.ShowModal() == wx.ID_OK:
            if filename:
                name = filename
            else:
                name = dlg.GetPath()
                # self.set("last_file_filter", dlg.GetFilterIndex())
                dlg.Destroy()

            if not os.path.exists(name):
                self.statusbar.SetStatusText("File not found!")
                return

            try:
                print(const.recent_files_path)
                abspath = os.path.abspath(name)
                recent_files = []
                try:
                    with open(const.recent_files_path, 'r') as file:
                        recent_files = json.load(file)
                except FileNotFoundError as exc:
                    logging.exception("Failed to load recent files list:\n", exc_info=exc)
                if abspath in recent_files:
                    recent_files.remove(abspath)
                recent_files.insert(0, abspath)
                if len(recent_files) > 5:
                    recent_files = recent_files[:5]

                with open(const.recent_files_path, 'w') as file:
                    file.write(json.dumps(recent_files))
            except FileExistsError as exc:
                logging.exception("Could not update recent files list:\n", exc_info=exc)
            if name.lower().endswith(".stl") or name.lower().endswith(".obj"):
                # if file consist 3D model -> to do slice it
                self.slice(name)
            else:
                # self.load_gcode_async(name)
                pass
        else:
            dlg.Destroy()

    def on_exit(self, event):
        """This function simply generates a wxCloseEvent whose handler usually
        tries to close the window."""
        self.Close()

    def load_recent_file(self, event):
        """This function simply load recent file with 3D-model."""
        fileid = event.GetId() - wx.ID_FILE1
        path = self.file_history.GetHistoryFile(fileid)
        self.load_file(None, filename=path)

    def update_recent_files(self):
        if self.file_history is None:
            return
        recent_files = []
        try:
            with open(const.recent_files_path, 'r') as file:
                recent_files = json.load(file)
        except FileNotFoundError as exc:
            # logging.warning("Failed to load recent files list:" + "\n" + str(exc))
            logging.warning("Failed to load recent files list:\n", exc_info=exc)
        # Clear history
        while self.file_history.GetCount():
            self.file_history.RemoveFileFromHistory(0)
        recent_files.reverse()
        for file in recent_files:
            self.file_history.AddFileToHistory(file)

    def slice(self, filename):
        # TODO create func slice
        """
        Slice 3D model:
            -.stl
            -.obj
        :param filename: path to file
        :return:
        """

    def about(self, event):
        """Show about dialog"""
        info = wx.adv.AboutDialogInfo()
        info.SetName(' Put Pen to Paper')
        info.SetVersion(const.version)
        info.SetDescription("PPP is a pure Python 3D printing host software.")
        info.SetCopyright('(C) 2017 - 2019')
        info.SetWebSite('asxing@rambler.ru')

        licence = """PPP is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
PPP. If not, see <http://www.gnu.org/licenses/>."""
        info.SetLicence(licence)
        info.AddDeveloper('Andrei Du')
        wx.adv.AboutBox(info)


class App(wx.App):
    """Main class that contains all the elements."""
    main_window = None

    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)
        self.main_window = FullWindow()
        self.main_window.Show()


# For tests Run Application
if __name__ == '__main__':

    APP = App(False)
    try:
        APP.MainLoop()
    except KeyboardInterrupt:
        logging.error("without GUI")
    del APP
