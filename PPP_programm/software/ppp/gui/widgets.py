# This file is part of the PPP suite.
#
# PPP is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PPP.  If not, see <http://www.gnu.org/licenses/>.

import wx


class PrinterOptionsDialog(wx.Dialog):
    """Options editor"""

    def __init__(self, pronterface):
        wx.Dialog.__init__(self, parent=None, title="Edit settings",
                           size=(400, 500), style=wx.DEFAULT_DIALOG_STYLE)
        panel = wx.Panel(self)
        header = wx.StaticBox(panel, label="Settings")
        sbox = wx.StaticBoxSizer(header, wx.VERTICAL)
        notebook = wx.Notebook(panel)
        all_settings = pronterface.settings._all_settings()
        group_list = []
        groups = {}
        for group in ["Printer", "UI", "Viewer", "Colors", "External"]:
            group_list.append(group)
            groups[group] = []
        for setting in all_settings:
            if setting.group not in group_list:
                group_list.append(setting.group)
                groups[setting.group] = []
            groups[setting.group].append(setting)
        for group in group_list:
            grouppanel = wx.Panel(notebook, -1)
            # notebook.AddPage(grouppanel, SETTINGS_GROUPS[group])
            settings = groups[group]
            grid = wx.GridBagSizer(hgap=8, vgap=2)
            current_row = 0
            for setting in settings:
                if setting.name.startswith("separator_"):
                    sep = wx.StaticLine(grouppanel, size=(-1, 5), style=wx.LI_HORIZONTAL)
                    grid.Add(sep, pos=(current_row, 0), span=(1, 2),
                             border=3, flag=wx.ALIGN_CENTER | wx.ALL | wx.EXPAND)
                    current_row += 1
                label, widget = setting.get_label(grouppanel), setting.get_widget(grouppanel)
                if setting.name.startswith("separator_"):
                    font = label.GetFont()
                    font.SetWeight(wx.BOLD)
                    label.SetFont(font)
                grid.Add(label, pos=(current_row, 0),
                         flag=wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT)
                grid.Add(widget, pos=(current_row, 1),
                         flag=wx.ALIGN_CENTER_VERTICAL | wx.EXPAND)
                if hasattr(label, "set_default"):
                    label.Bind(wx.EVT_MOUSE_EVENTS, label.set_default)
                    if hasattr(widget, "Bind"):
                        widget.Bind(wx.EVT_MOUSE_EVENTS, label.set_default)
                current_row += 1
            grid.AddGrowableCol(1)
            grouppanel.SetSizer(grid)
        sbox.Add(notebook, 1, wx.EXPAND)
        panel.SetSizer(sbox)
        topsizer = wx.BoxSizer(wx.VERTICAL)
        topsizer.Add(panel, 1, wx.ALL | wx.EXPAND)
        topsizer.Add(self.CreateButtonSizer(wx.OK | wx.CANCEL), 0, wx.ALIGN_RIGHT)
        self.SetSizerAndFit(topsizer)
        self.SetMinSize(self.GetSize())


def PrinterOptions(pronterface):
    dialog = PrinterOptionsDialog(pronterface)
    if dialog.ShowModal() == wx.ID_OK:
        for setting in pronterface.settings._all_settings():
            old_value = setting.value
            setting.update()
            if setting.value != old_value:
                pronterface.set(setting.name, setting.value)
    dialog.Destroy()
